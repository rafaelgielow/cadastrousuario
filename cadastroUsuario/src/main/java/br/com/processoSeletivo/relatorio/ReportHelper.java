package br.com.processoSeletivo.relatorio;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ReportHelper {

	public enum FORMATO_RELATORIO {
		XLS, PDF;
	}

	private String gerarIdReport() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyyhhmmss");
		String id = simpleDateFormat.format(new Date());

		return id;
	}

	/**
	 * Retorna caminho onde os relatórios (.jasper e .jrxml e tmps) ficam
	 * armazenados
	 */
	private String reportSourcePath() {

		return FacesContext.getCurrentInstance().getExternalContext().getRealPath("/WEB-INF/report/") + "/";
	}

	/**
	 * Gera o relatório e retorna o nome do relatório gerado
	 */
	private void gerarRelatorio(String relatorio, List beans, Map<String, Object> params,
			FORMATO_RELATORIO formatoExportacao) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
		    HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
		    ServletOutputStream outputStream = response.getOutputStream();
			
			if (params == null) {
				params = new HashMap<String, Object>();
			}

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(beans);

			String relatorioFormated = relatorio.endsWith(".jasper") ? relatorio : (new StringBuilder()).append(relatorio).append(".jasper").toString();
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(reportSourcePath() + relatorioFormated, params,
						beanCollectionDataSource);
			
			StringBuilder nomeRelatorio = new StringBuilder();
			nomeRelatorio.append(relatorio + "_" + gerarIdReport());

			if (formatoExportacao.equals(FORMATO_RELATORIO.PDF)) {
				response.setContentType("application/pdf");
				nomeRelatorio.append(".pdf");
			} else if (formatoExportacao.equals(FORMATO_RELATORIO.XLS)) {
				response.setContentType("application/excel");
				nomeRelatorio.append(".xls");
			}
			
	        // Ajusta o response
			response.addHeader("Content-Disposition", "attachment; filename=\""+ nomeRelatorio + "\"");
			
	        // Escreve no OutputStream do response
	        JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
	        fc.responseComplete();
	        
		} catch (JRException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Geração de Relatório em XLS
	 */
	public void gerarRelatorioXLS(String relatorio, List beans, Map<String, Object> params) {

		gerarRelatorio(relatorio, beans, params, FORMATO_RELATORIO.XLS);

	}

	/**
	 * Padrão para Arquivos PDF
	 */
	public void gerarRelatorio(String relatorio, List beans, Map<String, Object> params) {

		gerarRelatorio(relatorio, beans, params, FORMATO_RELATORIO.PDF);

	}

}