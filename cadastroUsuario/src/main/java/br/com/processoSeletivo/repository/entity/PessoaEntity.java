package br.com.processoSeletivo.repository.entity;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
 
 
@Entity
@Table(name="pessoa")
@NamedQueries({
	@NamedQuery(name= "PessoaEntity.findAll",query="SELECT p FROM PessoaEntity p")
})
public class PessoaEntity {
 
	@Id
	@GeneratedValue
	@Column(name = "pess_cd_id")
	private Integer codigo;
 
	@Column(name = "pess_tx_nome")
	private String  nome;
 
	@Column(name = "pess_cd_sexo")
	private String  sexo;
 
	@Column(name = "pess_dt_inclusao")
	private Date	dataCadastro;
 
	@Column(name = "pess_tx_email")
	private String  email;
 
	@Column(name = "pess_tx_endereco")
	private String  endereco;
 
	@OneToOne
	@JoinColumn(name = "pess_cd_usuario_inclusao", nullable = false, referencedColumnName = "usua_cd_id")
	private UsuarioEntity usuarioEntity;
 
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public UsuarioEntity getUsuarioEntity() {
		return usuarioEntity;
	}
	public void setUsuarioEntity(UsuarioEntity usuarioEntity) {
		this.usuarioEntity = usuarioEntity;
	}
 
}