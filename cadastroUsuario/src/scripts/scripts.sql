CREATE DATABASE db_processo_seletivo;

CREATE TABLE db_processo_seletivo.usuario(
 
	usua_cd_id INT AUTO_INCREMENT PRIMARY KEY NOT NULL COMMENT 'CÓDIGO DO USUÁRIO',
	usua_tx_login   VARCHAR(30) NOT NULL COMMENT 'LOGIN DO USUÁRIO PARA ACESSO AO SISTEMA',
	usua_tx_senha   VARCHAR(30) NOT NULL COMMENT 'SENHA DO USUÁRIO PARA ACESSO AO SISTEMA'   	
 
);

CREATE TABLE db_processo_seletivo.pessoa(
 
    pess_cd_id          			INT AUTO_INCREMENT PRIMARY KEY NOT NULL COMMENT 'CÓDIGO DA PESSOA',
    pess_tx_nome        			VARCHAR(70)  NOT NULL COMMENT 'NOME DA PESSOA',
    pess_cd_sexo        			CHAR(1)	     NOT NULL COMMENT 'INFORMAR M OU F',
    pess_dt_inclusao    			DATETIME     NOT NULL COMMENT 'DATA DE CADASTRO DO REGISTRO',
    pess_tx_email       			VARCHAR(80)  NOT NULL COMMENT 'EMAIL DA PESSOA',
    pess_tx_endereco    			VARCHAR(200) NOT NULL COMMENT 'DESCRIÇÃO DO ENDEREÇO',
    pess_cd_usuario_inclusao 		INT NOT NULL COMMENT  'USUÁRIO LOGADO QUE CADASTROU A PESSOA'
 
);

ALTER TABLE db_processo_seletivo.pessoa ADD FOREIGN KEY (pess_cd_usuario_inclusao) REFERENCES db_processo_seletivo.usuario(usua_cd_id);

INSERT INTO db_processo_seletivo.usuario (usua_tx_login,usua_tx_senha) VALUES('admin','123456');